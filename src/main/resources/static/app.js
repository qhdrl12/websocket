var stompClient = null;

function setConnected(connected) {
	$("#connect").prop("disabled", connected);
	$("#disconnect").prop("disabled", !connected);
	if (connected) {
		$("#conversation").show();
	} else {
		$("#conversation").hide();
	}
	$("#response").html("");
}

function connect() {
	var socket = new SockJS('http://127.0.0.1:9090/wsapi');
	console.log("socket : " + JSON.stringify(socket));
	var headers = {
//		'X-Client-UUID' : 'DV001',
//		'X-Client-Type' : 'MY_APPS',
//		'X-Auth-Timestamp' : '2017-06-30T09:00:00.000Z0001',
//		'X-Auth-Signature' : 'test'
	}
	stompClient = Stomp.over(socket);
	stompClient.connect(headers, function(frame) {
		setConnected(true);
		console.log('Connected: ' + frame);
		/* stompClient.subscribe('/topic/developerMode', function(greeting) {
			showGreeting(JSON.parse(greeting.body).content);
		}); */
		stompClient.subscribe('/topic/appGallery', function(greeting) {
			showGreeting(JSON.parse(greeting.body).result_cd);
		});
		stompClient.subscribe('/topic/developerMode/alaksjdfnasdf', function(greeting) {
//			console.log(JSON.parse(greeting.body).content["result_cd"]);
			showGreeting(JSON.parse(greeting.body).content);
		});
	}, function(message) {
		alert('Authentication failed. disconnect');
	});
}
 
function disconnect() {
	if (stompClient != null) {
		stompClient.disconnect();
	}
	setConnected(false);
	console.log("Disconnected");
}

function sendAuthCodeReq() {
	var sendMessage = {
		'appId' : $('#name').val(),
		'uuid' : 'alaksjdfnasdf'
	}
//	developerMode
//	stompClient.send("/app/developerMode", {}, JSON.stringify(sendMessage));
	stompClient.send("/app/developerMode", {}, JSON.stringify(sendMessage));
}

function showGreeting(message) {
	$("#response").append("<tr><td>" + message + "</td></tr>");
}

$(function() {
	$("form").on('submit', function(e) {
		e.preventDefault();
	});
	$("#connect").click(function() {
		connect();
	});
	$("#disconnect").click(function() {
		disconnect();
	});
	$("#send").click(function() {
		sendAuthCodeReq();
	});
});
