package com.uangel.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GgenieWebsocketApplication {
	public static void main(String[] args) {
		SpringApplication.run(GgenieWebsocketApplication.class, args);
	}
}
