package com.uangel.com;

public class AuthCodeMessage {
	public AuthCodeMessage() {
		super();
	}

	public AuthCodeMessage(String appId, String uuid) {
		super();
		this.appId = appId;
		this.uuid = uuid;
	}

	private String appId;

	private String uuid;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
