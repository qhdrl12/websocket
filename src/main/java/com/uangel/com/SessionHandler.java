package com.uangel.com;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

@Service
public class SessionHandler {
	private static final Logger logger = LoggerFactory.getLogger(SessionHandler.class);

	private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

	private final Map<String, WebSocketSession> sessionMap = new ConcurrentHashMap<>();

	public SessionHandler() {
		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				sessionMap.keySet().forEach(k -> {
					try {
						sessionMap.get(k).close();
						sessionMap.remove(k);
						System.out.println("Websocket connection was closed (after 300 sec). [" + k + "]");
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
			}
		}, 300, 300, TimeUnit.SECONDS);
	}

	public void register(WebSocketSession session) {
		sessionMap.put(session.getId(), session);
	}

	public void closeWebSocketSession(String k) {
		try {
			sessionMap.get(k).close();
			sessionMap.remove(k);
			System.out.println("Websocket connection was closed. [" + k + "]");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
