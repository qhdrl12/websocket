package com.uangel.com;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;

public class ChannelIntercepter extends ChannelInterceptorAdapter {
	@Autowired
	private SessionHandler sessionHandler;

	@Override
	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
		System.out.println("message=" + message);
		System.out.println("accessor=" + accessor);

		MessageHeaders headers = message.getHeaders();
		SimpMessageType type = (SimpMessageType) headers.get("simpMessageType");

		if (type == SimpMessageType.CONNECT) {
			List<String> tmp = accessor.getNativeHeader("X-Client-UUID");
			String xClientUuid;

			if (tmp == null || tmp.size() == 0) {
				System.out.println("xClientUuid is null");
			} else {
				xClientUuid = tmp.get(0);
				System.out.println("xClientUuid=" + xClientUuid);

				if ("test1".equals(xClientUuid)) {
					System.out.println("sessionId=" + accessor.getSessionId());
					sessionHandler.closeWebSocketSession(accessor.getSessionId());
				} else if ("test2".equals(xClientUuid)) {
					System.out.println("sessionId=" + accessor.getSessionId());
				}
			}
		}

		return super.preSend(message, channel);
	}
}
