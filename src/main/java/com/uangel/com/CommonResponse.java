package com.uangel.com;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.uangel.platform.collection.BaseObject;
import com.uangel.platform.collection.JsonObject;

/**
 * ================================================================================
 * @Project      : ggenie-core
 * @Package      : com.uangel.ggenie.core.common.model
 * @Filename     : CommonResponse.java
 *
 * All rights reserved. No part of this work may be reproduced, stored in a
 * retrieval system, or transmitted by any means without prior written
 * permission of UANGEL Inc.
 *
 * Copyright(c) 2017 UANGEL All rights reserved
 * =================================================================================
 *  No     DATE              Description
 * =================================================================================
 *  1.0	   2017. 6. 20.      Initial Coding & Update
 * =================================================================================
 */
public class CommonResponse extends BaseObject {
	private int result_cd;
	private String result_msg;
	@JsonInclude(value = Include.NON_NULL)
	private String trxid;

	public CommonResponse() {
		this.result_cd = BaseErrorMsg.OK.getErrorCode();
		this.result_msg = BaseErrorMsg.OK.getErrorMsg();
	}

	public CommonResponse(int cd, String msg) {
		this.result_cd = cd;
		this.result_msg = msg;
	}

	public int getResult_cd() {
		return this.result_cd;
	}

	public void setResult_cd(int result_cd) {
		this.result_cd = result_cd;
	}

	public String getResult_msg() {
		return this.result_msg;
	}

	public void setResult_msg(String result_msg) {
		this.result_msg = result_msg;
	}

	public String getTrxid() {
		return this.trxid;
	}

	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}

	@Override
	public String toString() {
		// return this.toJsonObject().toString();
		return new JSONObject(this.toJsonObject()).toString();
	}
	
	public JsonObject toJsonObject() {
		JsonObject jsonObj = new JsonObject();

		jsonObj.set("result_cd", this.result_cd);
		jsonObj.set("result_msg", this.result_msg);

		return jsonObj;
	}
}
