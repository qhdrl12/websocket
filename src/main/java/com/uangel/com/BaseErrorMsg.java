package com.uangel.com;

public enum BaseErrorMsg {
	// Common
	OK(200, 200, "Success"),
	NotFoundMandatoryParam(200, 400, "Necessary Parameters does not set"),
	ServerError(200, 500, "Internal Server Error"),
	AppNotFound(200, 404, "App is not found"),
	OperationKeyNotFound(200, 404, "Operation key is not found"),
	DuplicateKeyError(200, 409, "Duplicate Key Error"),
	InvalidParameter(200, 400, "Invalid Parameter"),
	InvalidBody(200, 400, "Invalid Body"),
	InvalidFile(200, 400, "Invalid File"),
	Unauthorized(200, 401, "Unauthorized"),
	NotValidSignature(200, 403, "Not Valid Signature"),
	ReplayAttack(200, 406, "Replay Attack"),
	NotFoundMandatoryHeader(200, 412, "Precondition Failed"),

	InvalidSystemId(200, 401, "Invalid System Id"),

	ResTypeNotFound(200, 404, "Resource type is not found"),

	// AppKey 검증
	AppKeyOK(200, 200, "Valid Key"),
	AppNotFoundMandatoryParam(200, 400, "Necessary Parameters does not set"),
	AppNotFoundKey(200, 404, "Key is not found"),
	AppInvalidKeyType(200, 415, "Invalid Key Type"),
	AppInvalidKey(200, 498, "Invalid Key"),

	// Profile 검증
	ProfileVersionNotModified(200, 304, "Version is not modified"),
	ProfileVersionNotFound(200, 404, "Version Information is not found"),

	// Device 검증
	DeviceUuidNotFound(200, 404, "UUID is not found"),

	HasLastestVersionApp(200, 400, "The lastest version WAR already uploaded");

	private int statusCode;
	private int errorCode;
	private String errorMsg;

	BaseErrorMsg(final int statusCode, final int errorCode, final String errorMsg) {
		this.statusCode = statusCode;
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public int getErrorCode() {
		return this.errorCode;
	}

	public String getErrorMsg() {
		return this.errorMsg;
	}

	@Override
	public String toString() {
		return this.errorMsg;
	}
}
