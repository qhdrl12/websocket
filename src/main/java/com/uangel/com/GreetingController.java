package com.uangel.com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@EnableScheduling
@Controller
public class GreetingController {
	@Autowired
	private SimpMessagingTemplate template;

	@MessageMapping("/developerMode")
	// @SendTo("/topic/developerMode/alaksjdfnasdf")
	public void greeting(AuthCodeMessage message) throws Exception {
		Thread.sleep(1000);
		System.out.println("appId=" + message.getAppId());
		System.out.println("uuid=" + message.getUuid());

		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setResult_cd(200);
		commonResponse.setResult_msg("Success");

		Greeting greeting = new Greeting(commonResponse.toString());

		System.out.println(commonResponse.toString());

		this.template.convertAndSend("/topic/developerMode/" + message.getUuid(), greeting);
		// return greeting;
	}

	@RequestMapping("/appGallery")
	public @ResponseBody void callGreeting() {
		System.out.println("broadcasting app gallery");

		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setResult_cd(200);
		commonResponse.setResult_msg("Success");

		Greeting greeting = new Greeting(commonResponse.toString());

		System.out.println(commonResponse.toString());

		this.template.convertAndSend("/topic/appGallery", greeting);
	}

	@RequestMapping("/developerMode/{uuid}")
	public @ResponseBody void authCodeSubmit(@PathVariable("uuid") String uuid) {
		System.out.println("replying developer mode");

		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setResult_cd(200);
		commonResponse.setResult_msg("authorized");

		Greeting greeting = new Greeting(commonResponse.toString());

		System.out.println(commonResponse.toString());

		this.template.convertAndSend("/topic/developerMode/" + uuid, greeting);
	}
}
